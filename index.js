require("dotenv").config();
const { Telegraf } = require("telegraf");
const fetch = require("node-fetch");
const moment = require('moment')
const bot = new Telegraf(process.env.BOT_TOKEN);
const sheet = process.env.SHEET || '';
const apiHorario = `http://worldtimeapi.org/api/timezone/America/Sao_Paulo`;
const horarioOffset = -3;

const programacaoTabela = `<pre>
|     | Sábado 03/04                        | Domingo 04/04                      | 
|---- | ----------------------------------- | ---------------------------------- | 
|10:00| Começa Autohospedagem               | @hlupes EAD versus Pedagogia       | 
|11:00| @brunovianna Yunohost/Funkwhale     | Dai (@araujodai) – Etnomatemática  |
|12:00| Almoço                              | Almoço                             |
|13:00| Almoço                              | Almoço                             |
|14:00| @fernao – Certificados Digitais     | @fernao – OpenVPN Selfhosted       |
|15:00| Radio comunitaria  @ArlekinoDeIoio  |  @fernao – Jitsi Selfhosted        |
|16:00| @issonaoserve - Comecar uma rede    |  @hiure  – Kolibri/Ferramentas EAD |
|17:00| @issonaoserve - Redes Mesh tec      |  @luandro - p2p                    |
|18:00| Todas pessoas - Roda de encontro    |  Todas pessoas - Finalizacao       |
</pre>`;

const programacao = `
---------------------------
**Domingo 04/04**
---------------------------
- 10:00 @hlupes EAD versus Pedagogia
- 11:00 Dai (@araujodai) – Etnomatemática
- 12:00 Horário Aberto 
- 13:00 Horário Aberto 
- 14:00 @fernaovellozo – OpenVPN Selfhosted
- 15:00 @fernaovellozo – Jitsi Selfhosted      
- 16:00 @hiure  – Kolibri/Ferramentas EAD 
- 17:00 @luandro - p2p  
- 18:00 Todas pessoas - Finalizacao
`;

const welcomeSwitch = async (ctx) => {
  console.log(
    "------------------------------------------------------------------------"
  );
  if (!ctx.message) return null;
  switch (ctx.message.text.split(`\n`)[0]) {
    case "/programação":
      return ctx.replyWithMarkdown(programacao);
    case "/salas":
      return ctx.replyWithMarkdown(
        `Temos vários espaços abertos para que possam se encontrar:

**A programação principal ta rolando aqui: https://meet.jit.si/coolabcamp

**Discord: Mirc com audio**: https://discord.gg/uEWUykera3

**Work Adventure: Tibia com Jitsi**: https://play.workadventu.re/@/lhc/2d-hackerspace/lhc-online

**Wonder: Bolinhas de video**: https://www.wonder.me/r?id=431cc65d-5970-4bfb-a54a-8cdd4c89d05a
        `,
        {
          disable_web_page_preview: true,
        }
      );
    // case "/agora":
    //   ctx.replyWithMarkdown(`Vou buscar na nossa programação...`);
    //   // const horaAgora = await fetch(apiHorario);
    //   // const horaJson = await horaAgora.json();
    //   // const dateAgora = horaJson.unixtime;
    //   const data = await fetch(sheet);
    //   const json = await data.json();
    //   const agora = json.filter((item, index) => {
    //     if (index === 0) return;
    //     const split = item[0].split("h");
    //     const itemHora = parseInt(split[0]);
    //     const itemMin = parseInt(split[1]);
    //     const timeNow = moment()
    //     console.log("TIME NOW", time)
    //     console.log('TIME IN ', )
    default:
      return null;
  }
};

bot.start(async (ctx) => {
  await ctx.reply("Bem vind@ ao Coolab Camp 2021");
  await ctx.reply("Digite /programação para ver a programação do evento");
  await ctx.reply("Estamos nós reunindo em: https://meet.jit.si/coolabcamp");
  return ctx.reply("Digite /salas para ver outras salas. Bom evento.");
});

bot.on("message", (ctx) => {
  welcomeSwitch(ctx);
});

bot.launch();

// Enable graceful stop
process.once("SIGINT", () => bot.stop("SIGINT"));
process.once("SIGTERM", () => bot.stop("SIGTERM"));
